angular.module('personalSite', ['ngResource', 'ngRoute', 'firebase'])

  .config(function($routeProvider) {
    $routeProvider
      .when('/', {
        controller:'ResumeCtrl as resume',
        templateUrl:'resume.html'
      })
      .when('/sign-in', {
        controller:'SignInCtrl as signIn',
        templateUrl:'sign-in.html'
      })
      .when('/admin', {
        controller:'AdminCtrl as admin',
        templateUrl:'admin.html'
      })
      .otherwise({
        redirectTo:'/'
      });
  })

  .controller('ResumeCtrl', function($log) {
    $log.debug('Loading ResumeCtrl as resume');
    var resume = this;

    resume.workHistory = [
      {
        position:'Director of Product Development and Design',
        employer: 'TORQworks',
        startDate: 'Jan, 2015',
        endDate: 'Current',
        description: 'Lead product design and serve as the primary front-end developer. Built a responsive web application used by workforce agencies, vocational counselors, and job seekers.',
        bulletPoints: [
          'Built a web application using AngularJS',
          'Designed app from blank page to production',
          'Built and validated several prototypes'
        ]
      },
      {
        position:'Director of Product',
        employer: 'ACT Bridge',
        startDate: 'Dec, 2010',
        endDate: 'Dec, 2014',
        description: 'Led a pivot of the core business from consulting to SaaS. Built a web application used by recruiters and HR teams at large organizations. ACT Bridge was a startup funded by ACT, the college admissions testing company.',
        bulletPoints: [
          'Designed our enterprise, SaaS product from scratch',
          'Acted as product manager in an agile development environment',
          'Implemented our product for several enterprise customers'
        ]
      },
      {
        position:'Graduate Assistant',
        employer: 'New Media Institute, University of Georgia',
        startDate: 'Aug, 2009',
        endDate: 'Dec, 2010',
        description: 'Assistant to the Director of the New Media Institute, Dr. Scott Shamp, while in graduate school completing coursework for a Masters in Mass Media from the Grady College of Journalism and Mass Communication.',
        bulletPoints: [
          'TA for a 300+ student course, Intro to New Media',
          'TA for the New Media capstone courses',
          'Founded a student-group called New Media Society',
          'Founded the TEDxUGA organization'
        ]
      }
    ];

    resume.educationHistory = [
      {
        degree: 'B.A. - Cognitive Science',
        school: 'University of Georgia',
        date: 'May, 2009',
        description: 'New Media Interdisciplinary Certification, Mobile Media Scholar, Hugh Hodgson School of Music Award, Business Manager and tenor in the award-winning, all-male a cappella group the UGA Accidentals'
      },
      {
        degree: 'Honors Graduate',
        school: 'Mauldin High School',
        date: 'May, 2004',
        description: 'Student Body Co-President, Key Club President, Varsity Cross Country, Varsity Soccer, Speech & Debate Club, Chorus, Band, SAT team, and a Youth-in-Government Mock Trail Attorney'
      }
    ];

    resume.projects = [
      {
        name: 'Assistance League Events App',
        position: 'Front-end Developer',
        startDate: 'Dec, 2015',
        endDate: 'Jun, 2016',
        description: 'Volunteer project with a few Seattle-based developer friends. Built an app to help a volunteer organization manage seasonal events in which they help students who have been touched by hardship or violence purchase essential school supplies.'
      },
      {
        name: 'The Graduates',
        position: 'Group Director, Beatboxer, Tenor',
        startDate: 'Sep, 2011',
        endDate: 'Dec, 2014',
        description: 'Enjoyed singing, beatboxing, and volunteering as group director in this Atlanta-based, co-ed a cappella group.'
      },
      {
        name: 'STAT MD',
        position: 'Product Designer and Front-end Developer',
        startDate: 'Aug, 2013',
        endDate: 'Jul, 2014',
        description: 'Side project with a few close friends. We built an app to help doctors find the right hospital after their residency. Built with Bootstrap, Ruby on Rails, and PhoneGap.'
      },
      {
        name: 'One Laptop Per Child in Costa Rica',
        position: 'Project Lead',
        startDate: null,
        endDate: 'Jul, 2008',
        description: 'A personal, solo project. Raised funds to purchase 13 laptops from OLPC, traveled to a remote elementary school in Costa Rica, taught the students how to use the laptops for school and play.'
      }
    ];

    resume.skills = ['HTML', 'CSS', 'JavaScript', 'Angular', 'Product Design', 'Mockups', 'Prototyping', 'Web Design'];


  })

  .controller('SignInCtrl', function($log, $location) {
    $log.debug('Loading SignInCtrl as signIn');
    var signIn = this;
    signIn.authenticate = function(credentials) {
      $log.debug('Trying to sign in user', credentials.email);
      $location.path('/admin');
    };
  })

  .controller('AdminCtrl', function($log, $location) {
    $log.debug('Loading AdminCtrl as admin');
    var admin = this;
    admin.signOut = function() {
      $log.debug('Signing out');
      $location.path('/');
    };
  });
